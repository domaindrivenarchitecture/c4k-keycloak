(ns dda.c4k-keycloak.core-test
  (:require
   #?(:clj [clojure.test :refer [deftest is are testing run-tests]]
      :cljs [cljs.test :refer-macros [deftest is are testing run-tests]])
   [clojure.spec.alpha :as s]
   [clojure.spec.test.alpha :as st]
   [dda.c4k-common.yaml :as yaml]
   [dda.c4k-keycloak.core :as cut]
    #?(:cljs [dda.c4k-common.macros :refer-macros [inline-resources]])))

(st/instrument `cut/onfig-objects)
(st/instrument `cut/auth-objects)

#?(:cljs
   (defmethod yaml/load-resource :keycloak-test [resource-name]
     (get (inline-resources "keycloak-test") resource-name)))

(deftest validate-valid-resources
  (is (s/valid? cut/config? (yaml/load-as-edn "keycloak-test/valid-config.yaml")))
  (is (s/valid? cut/auth? (yaml/load-as-edn "keycloak-test/valid-auth.yaml")))
  (is (not (s/valid? cut/config? (yaml/load-as-edn "keycloak-test/invalid-config.yaml"))))
  (is (not (s/valid? cut/auth? (yaml/load-as-edn "keycloak-test/invalid-auth.yaml")))))

(deftest test-whole-generation
  (is (= 32
         (count
          (cut/config-objects
           (yaml/load-as-edn "keycloak-test/valid-config.yaml")))))
  (is (= 3
         (count
          (cut/auth-objects
           (yaml/load-as-edn "keycloak-test/valid-config.yaml")
           (yaml/load-as-edn "keycloak-test/valid-auth.yaml"))))))