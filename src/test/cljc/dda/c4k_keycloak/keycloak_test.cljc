(ns dda.c4k-keycloak.keycloak-test
  (:require
   #?(:clj [clojure.test :refer [deftest is are testing run-tests]]
      :cljs [cljs.test :refer-macros [deftest is are testing run-tests]])
   [clojure.spec.test.alpha :as st]
   [dda.c4k-keycloak.keycloak :as cut]))

(st/instrument `cut/generate-configmap)
(st/instrument `cut/generate-deployment)
(st/instrument `cut/generate-secret)
(st/instrument `cut/config)

(deftest should-generate-configmap
  (is (= "keycloak"
         (get-in 
          (cut/generate-configmap {:namespace "keycloak" :fqdn "test.de"
                                   :max-rate 10
                                   :max-concurrent-requests 5}) 
          [:metadata :namespace]))))

(deftest should-generate-config
  (is (= 4
         (count (cut/config {:fqdn "example.com" :namespace "keycloak"
                             :max-rate 10
                             :max-concurrent-requests 5})))))

(deftest should-generate-secret
  (is (= {:apiVersion "v1"
          :kind "Secret"
          :metadata {:name "keycloak", :namespace "keycloak"
                     :labels #:app.kubernetes.io{:name "keycloak", :component "keycloak"}}
          :type "Opaque"
          :data
          {:KEYCLOAK_DATABASE_PASSWORD "ZGItcGFzc3dvcmQ=",
           :KEYCLOAK_DATABASE_USER "a2V5Y2xvYWs=",
           :KC_BOOTSTRAP_ADMIN_PASSWORD "cGFzc3dvcmQ=",
           :KC_BOOTSTRAP_ADMIN_USERNAME "dXNlcg=="}}
         (cut/generate-secret {:namespace "keycloak" :fqdn "test.de"
                               :max-rate 10
                               :max-concurrent-requests 5}
                              {:keycloak-admin-user "user" :keycloak-admin-password "password"
                               :postgres-db-user "keycloak"
                               :postgres-db-password "db-password"}))))