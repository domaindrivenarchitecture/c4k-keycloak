(ns dda.c4k-keycloak.core
 (:require
  [clojure.spec.alpha :as s]
  #?(:clj [orchestra.core :refer [defn-spec]]
     :cljs [orchestra.core :refer-macros [defn-spec]])
  [dda.c4k-common.common :as cm]
  [dda.c4k-common.predicate :as p]
  [dda.c4k-common.monitoring :as mon]
  [dda.c4k-common.yaml :as yaml]
  [dda.c4k-keycloak.keycloak :as kc]
  [dda.c4k-keycloak.backup :as backup]
  [dda.c4k-common.postgres :as postgres]
  [dda.c4k-common.namespace :as ns]))

(def config-defaults {:namespace "keycloak",
                      :issuer "staging"
                      :db-name "keycloak"
                      :pv-storage-size-gb 30
                      :pvc-storage-class-name :local-path
                      :postgres-image "postgres:14"
                      :postgres-size :2gb
                      :max-rate 100
                      :max-concurrent-requests 50})

(def config? (s/keys :req-un [::kc/fqdn]
                     :opt-un [::kc/issuer
                              ::kc/namespace
                              ::backup/restic-repository
                              ::mon/mon-cfg]))

(def auth? (s/keys :req-un [::kc/keycloak-admin-user 
                            ::kc/keycloak-admin-password
                            ::postgres/postgres-db-user 
                            ::postgres/postgres-db-password]
                   :opt-un [::backup/restic-password
                            ::backup/restic-new-password
                            ::backup/aws-access-key-id
                            ::backup/aws-secret-access-key
                            ::mon/mon-auth]))

(defn-spec config-objects p/map-or-seq?
  [config config?]
  (let [resolved-config (merge config-defaults config)]
    (map yaml/to-string
         (filter
          #(not (nil? %))
          (cm/concat-vec
           (ns/generate resolved-config)
           (postgres/generate-config resolved-config)
           (kc/config resolved-config)           
           (kc/generate-ratelimit-ingress resolved-config)
           (when (contains? resolved-config :restic-repository)
             [(backup/generate-config resolved-config)
              (backup/generate-cron)
              (backup/generate-backup-restore-deployment resolved-config)])
           (when (contains? resolved-config :mon-cfg)
             (mon/generate-config)))))))

(defn-spec auth-objects p/map-or-seq?
  [config config?
   auth auth?]
  (let [resolved-config (merge config-defaults config)]
    (map yaml/to-string
         (filter
          #(not (nil? %))
          (cm/concat-vec
           (postgres/generate-auth resolved-config auth)
           (kc/auth resolved-config auth)
           (when (contains? resolved-config :restic-repository)
             [(backup/generate-secret auth)])
           (when (and (contains? auth :mon-auth) (contains? resolved-config :mon-cfg))
             (mon/generate-auth (:mon-cfg resolved-config) (:mon-auth auth))))))))
