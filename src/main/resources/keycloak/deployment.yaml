apiVersion: apps/v1
kind: Deployment
metadata:
  name: keycloak
  namespace: NAMESPACE
  labels:
    app.kubernetes.io/name: keycloak
    app.kubernetes.io/component: keycloak
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: keycloak
      app.kubernetes.io/component: keycloak
  template:
    metadata:
      labels:
        app.kubernetes.io/name: keycloak
        app.kubernetes.io/component: keycloak
    spec:
      securityContext:
        fsGroup: 1001
        fsGroupChangePolicy: Always
        supplementalGroups: []
        sysctls: []
      enableServiceLinks: true
      initContainers:
        - name: prepare-write-dirs
          image: docker.io/bitnami/keycloak:26.1.3-debian-12-r0
          imagePullPolicy: IfNotPresent
          command:
            - /bin/bash
          args:
            - -ec
            - |
              . /opt/bitnami/scripts/liblog.sh

              info "Copying writable dirs to empty dir"
              # In order to not break the application functionality we need to make some
              # directories writable, so we need to copy it to an empty dir volume
              cp -r --preserve=mode /opt/bitnami/keycloak/lib/quarkus /emptydir/app-quarkus-dir
              cp -r --preserve=mode /opt/bitnami/keycloak/data /emptydir/app-data-dir
              cp -r --preserve=mode /opt/bitnami/keycloak/providers /emptydir/app-providers-dir
              info "Copy operation completed"
          securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            privileged: false
            readOnlyRootFilesystem: true
            runAsGroup: 1001
            runAsNonRoot: true
            runAsUser: 1001
            seLinuxOptions: {}
            seccompProfile:
              type: RuntimeDefault
          resources:
            limits:
              cpu: 750m
              ephemeral-storage: 2Gi
              memory: 768Mi
            requests:
              cpu: 500m
              ephemeral-storage: 50Mi
              memory: 512Mi
          volumeMounts:
           - name: empty-dir
             mountPath: /emptydir
      containers:
      - name: keycloak
        image: docker.io/bitnami/keycloak:26.1.3-debian-12-r0
        imagePullPolicy: IfNotPresent
        securityContext:
            allowPrivilegeEscalation: false
            capabilities:
              drop:
              - ALL
            privileged: false
            readOnlyRootFilesystem: true
            runAsGroup: 1001
            runAsNonRoot: true
            runAsUser: 1001
            seLinuxOptions: {}
            seccompProfile:
              type: RuntimeDefault
        env:
          - name: KUBERNETES_NAMESPACE
            valueFrom:
              fieldRef:
                apiVersion: v1
                fieldPath: metadata.namespace
          - name: BITNAMI_DEBUG
            value: "false"
          - name: KEYCLOAK_HTTP_RELATIVE_PATH
            value: "/"
          - name: KC_SPI_ADMIN_REALM
            value: "master"
        envFrom:
          - secretRef:
              name: keycloak
          - configMapRef:
              name: keycloak-env
        resources:
          limits:
            cpu: 750m
            ephemeral-storage: 2Gi
            memory: 768Mi
          requests:
            cpu: 500m
            ephemeral-storage: 50Mi
            memory: 512Mi
        ports:
          - name: http
            containerPort: 8080
            protocol: TCP
          - name: discovery
            containerPort: 7800
        livenessProbe:
          failureThreshold: 3
          initialDelaySeconds: 300
          periodSeconds: 1
          successThreshold: 1
          timeoutSeconds: 5
          tcpSocket:
            port: http
        readinessProbe:
          failureThreshold: 3
          initialDelaySeconds: 30
          periodSeconds: 10
          successThreshold: 1
          timeoutSeconds: 1
          httpGet:
            path: /realms/master
            port: http
        volumeMounts:
          - name: empty-dir
            mountPath: /tmp
            subPath: tmp-dir
          - name: empty-dir
            mountPath: /bitnami/keycloak
            subPath: app-volume-dir
          - name: empty-dir
            mountPath: /opt/bitnami/keycloak/conf
            subPath: app-conf-dir
          - name: empty-dir
            mountPath: /opt/bitnami/keycloak/lib/quarkus
            subPath: app-quarkus-dir
          - name: empty-dir
            mountPath: /opt/bitnami/keycloak/data
            subPath: app-data-dir
          - name: empty-dir
            mountPath: /opt/bitnami/keycloak/providers
            subPath: app-providers-dir
      volumes:
        - name: empty-dir
          emptyDir: {}
