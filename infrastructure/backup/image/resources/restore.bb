#!/usr/bin/env bb

(require '[babashka.tasks :as tasks]
         '[dda.backup.core :as bc]
         '[dda.backup.config :as cfg]
         '[dda.backup.postgresql :as pg]
         '[dda.backup.restore :as rs])

(def config (cfg/read-config "/usr/local/bin/config.edn"))

(def db-config (merge (:db-config config)
                      {:snapshot-id "latest"}))


(def dry-run {:dry-run true :debug true})

(defn prepare!
  []
  (bc/create-aws-credentials! (:aws-config config))
  (pg/create-pg-pass! db-config))

(defn restic-restore!
  []
  (pg/drop-create-db! db-config)
  (rs/restore-db! db-config))

(prepare!)
(restic-restore!)
